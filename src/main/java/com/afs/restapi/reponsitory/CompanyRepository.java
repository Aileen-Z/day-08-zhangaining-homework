package com.afs.restapi.reponsitory;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private List<Company> companies = new ArrayList<>();

    public CompanyRepository() {
        this.companies.add(new Company(1L, "COSCO"));
        this.companies.add(new Company(2L, "OOCL"));
        this.companies.add(new Company(3L, "CargoSmart"));
    }

    public List<Company> findAll() {
        return companies;
    }

    public Company findById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company insert(Company company) {
        company.setId(generateNewId());
        companies.add(company);
        return company;
    }

    private Long generateNewId() {
        return companies.stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L) + 1;
    }

    public Company update(Company company) {
        Company companyToUpdate = findById(company.getId());
        BeanUtils.copyProperties(company, companyToUpdate);
        return companyToUpdate;
    }


    public void delete(Company company) {
        companies.remove(company);
    }

    public void clearAll() {
        companies.clear();
    }
}
